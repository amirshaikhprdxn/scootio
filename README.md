prdxn-dev-framework
===================

PRDXN's "starting package" used for development.

1. In the banner section I have used placeholder, because image provided in assets were not proper.
2. There may be some issue related to pixel perfect, because the template screenshot is squeezed when we set the 
   width: 100%; and height: auto;